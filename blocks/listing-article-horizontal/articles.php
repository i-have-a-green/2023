<?php
/**
 * Block name: Listing article horizontal
 */

?>
    <?php
    global $post;
            $args = array(
                'post_type'         => 'post',
                'post_status'       => 'publish',
                'posts_per_page'    => 3,
            );
            
            $articles = get_posts( $args );
            ?>

                <div class="container-article-horizontal">
                    <?php
					if ( $articles ) :
						foreach ( $articles as $post ) :
							setup_postdata( $post ); 
							?>

                                    <article id="post-<?php the_ID(); ?>" class="article-horizontal">
                                        <div class="post-thumbnail">
                                            <a href="<?php echo esc_url( get_permalink() ); ?>">
                                                <?php the_post_thumbnail( 'thumbnail' ); ?>
                                            </a>
                                        </div>
                                        <div class="post-content">
                                            <?php the_title( '<h3 class="post-title"><a href="' . esc_url( get_permalink() ) . '">', '</a></h3>' ); ?>
                                            <div class="tags">
                                                <?php the_tags( '', '', '' ); ?>
                                            </div>
                                            <div class="entry-content">
                                                <?php the_excerpt(); ?>
                                            </div><!-- .entry-content -->
                                        </div>
                                        <div class="post-navigation">
                                            <a href="<?php echo esc_url( get_permalink() ); ?>" class="wp-block-button__link wp-element-button">Lire l'article</a>
                                        </div>
                                    </article>
                                
                                <?php
                            endforeach;
						wp_reset_postdata();
                        endif; 
                    ?>
                </div>
