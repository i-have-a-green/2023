<?php
/**
 * Block name: chiffre 2
 */

$template = array(
    array(
		'core/heading',
		array(
			'placeholder' => 'Chiffre',
			'level'       => 2,
		),
	),
    array(
		'core/paragraph',
		array(
			'placeholder' => 'Information',
		),
	),
    array(
		'core/paragraph',
		array(
			'placeholder' => 'source',
		),
	),
);

?>


<div class="number-card-2">
    <?php echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" />'; ?>
</div>
