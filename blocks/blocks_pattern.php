<?php
// remove default pattern
add_action('init', function() {
	remove_theme_support('core-block-patterns');
});

//add custom categories
function ihag_pattern_categories() {
    register_block_pattern_category(
        'global',
        array( 'label' => __( 'Global', 'inno3' ) )
    );
}
add_action( 'init', 'ihag_pattern_categories' );