<?php
/**
 * Block name: chiffre 1
 */

$template = array(
    array(
		'core/heading',
		array(
			'placeholder' => 'Title',
			'level'       => 2,
		),
	),
    array(
		'core/paragraph',
		array(
			'placeholder' => 'corps',
		),
	),
);

?>


<div class="number-card-1">
    <?php echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" />'; ?>
</div>
