<?php
// pour la newsletter
use \Mailjet\Resources;


/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action(
    'rest_api_init',
    function() {
		register_rest_route(
            'ihag',
            'newsletter-rest',
            array(
				'methods'               => 'POST', // WP_REST_Server::READABLE,
				'callback'              => 'ihagFormNewsletter',
				'permission_callback'   => array(),
				'args'                  => array(),
            )
		);
	
	}
);

function ihagFormNewsletter( WP_REST_Request $request ) {

	
	
	if ( empty( $_POST['honeypot'] ) && check_nonce() ) {
				
		// Newsletter
		if ( ! empty( $_POST['newletter_email'] ) ) :

			
			require 'vendor/autoload.php';
			
				$id_liste = get_field( 'id_liste', 'option' );
			
			$mj       = new \Mailjet\Client( get_field( 'id_mailjet', 'option' ), get_field( 'mdp_mailjet', 'option' ), true, array( 'version' => 'v3' ) );
			$body     = array(
				'Action'   => 'addnoforce',
				'Contacts' => array(
					array(
						'Email'                   => sanitize_text_field( $_POST['newletter_email'] ),
						'IsExcludedFromCampaigns' => 'false',
						'Name'                    => sanitize_text_field( $_POST['newletter_email'] ),
						'Properties'              => 'object',
					),
				),
			);
			$response = $mj->post(
                Resources::$ContactslistManagemanycontacts,
                array(
					'id'   => $id_liste,
					'body' => $body,
                ) 
            );
			$response->success(); 
			return new WP_REST_Response( '', 200 );

		endif;
	}       
	
	return new WP_REST_Response( '', 304 );
}
