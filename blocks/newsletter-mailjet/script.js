document.addEventListener('DOMContentLoaded', function () {
	if ( document.getElementById( 'newsletter-form-js' ) ) {
		const form = document.getElementById( 'newsletter-form-js' );
		form.addEventListener( 'submit', function ( e ) {
			e.preventDefault();
			e.stopPropagation();
			fetch( ihagRest.resturl + 'newsletter-rest', { 
				method: 'POST',
				body: new FormData( form ),
				headers: { 'X-WP-Nonce': ihagRest.nonce },
				cache: 'no-cache',
			} )
				.then( function ( response ) {
					if ( response.status !== 200 ) {
						document.getElementById( 'responseNews' ).innerHTML =
							'Cette adresse email est déjà enregistrée !';
						if (
							document.getElementsByTagName( 'html' )[ 0 ].lang ==
							'en-GB'
						) {
							document.getElementById( 'responseNews' ).innerHTML =
								'This email address is already registered !';
						}
						return;
					}
					response.json().then( function ( data ) {
						document.getElementById( 'submitNews' ).style.display =
							'none';
						document.getElementById( 'responseNews' ).innerHTML =
							"Votre adresse email vient d'être enregistrée !";
						if (
							document.getElementsByTagName( 'html' )[ 0 ].lang ==
							'en-GB'
						) {
							document.getElementById( 'responseNews' ).innerHTML =
								'Your email adress has been registered !';
						}
					} );
				} )
				.catch( function ( err ) {
					console.log( 'Fetch Error :-S', err );
				} );
		} );
	}
});