document.addEventListener('DOMContentLoaded', function () {
    const openModal = document.getElementById('show-modal');
    const closeModal = document.getElementById('close-modal');
    const favDialog = document.getElementById('fav-dialog');

    // If a browser doesn't support the dialog, then hide the
    // dialog contents by default.
    if (typeof favDialog.showModal !== 'function') {
        favDialog.hidden = true;
    }

    // affiche la modal
    openModal.addEventListener('click', () => {
        if (typeof favDialog.showModal === "function") {
            favDialog.showModal();
        }
    });

    // ferme la modal
    closeModal.addEventListener('click', () => {
        if (typeof favDialog.showModal === "function") {
            favDialog.close();
        }
    })
});