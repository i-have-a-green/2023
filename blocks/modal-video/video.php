<?php
/**
 * Block name : modal_video
 */

?>

<div class="video-container" id="data-container" data-video="<?php echo htmlentities( get_field( 'video' ) ); ?>">
    <dialog id="modal" role="dialog">
    </dialog>
    <div class="video-button">
        <img class="show-modal-video" src="<?php echo get_field( 'picture' ); ?>" alt="">
        <button class="show-modal-video">Voir la vidéo</button>
    </div>
</div>
