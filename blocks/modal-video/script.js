document.addEventListener('DOMContentLoaded', function () {

    // If a browser doesn't support the dialog, then hide the
    // dialog contents by default.
    if (typeof modal.showModal !== 'function') {
        modal.hidden = true;
    }

    let openModal = document.getElementsByClassName('show-modal-video');

    Array.prototype.forEach.call(openModal, function (el) {
        el.addEventListener('click', function(e) {
            e.stopPropagation();

            let modal = document.getElementById('modal');

            // créer un div pour tout les éléments de la modal
            let modalContainer = document.createElement('div');
            modalContainer.classList.add('modal-container');

            // créer le bouton de fermeture de la modal
            let modaleClose = document.createElement("button");
            modaleClose.classList.add('close-modal-video');
            modaleClose.innerText = 'x';

            //créer l'iframe 
            let videoContainer = document.createElement("div");
            let oembed = document.getElementById('data-container').dataset.video;
            videoContainer.classList.add('iframe-container');
            videoContainer.innerHTML = oembed;

            //ajout des élément de la modal au DOM
            modalContainer.appendChild(videoContainer);
            modalContainer.appendChild(modaleClose);
            modal.appendChild(modalContainer);

            document.body.style.overflow = 'hidden'

            //affiche la modal
            modal.showModal();

            //ferme la modal
            document.addEventListener('click', () => {
                if ( !videoContainer.contains(e.target) ) {
                    modal.close();
                    document.body.style.overflow = 'visible'
                }
            })

        })
    })
});