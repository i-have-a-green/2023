<?php

require_once get_template_directory() . '/blocks/blocks_pattern.php';
require_once get_template_directory() . '/blocks/blocks_style.php';

add_action( 'init', 'register_acf_blocks' );
function register_acf_blocks() {
    register_block_type( __DIR__ . '/listing-article' );
    register_block_type( __DIR__ . '/listing-article-horizontal' );
    register_block_type( __DIR__ . '/newsletter-mailchimp' );

    require_once( __DIR__ . '/newsletter-mailjet/mailjet.php' );
    register_block_type(
        __DIR__ . '/newsletter-mailjet',
        array(
            'script' => 'newsletter-mailjet-js',
        )
    );

    register_block_type( __DIR__ . '/formulaire-contact' );
    register_block_type( __DIR__ . '/bottom-single-nav' );
    register_block_type(
        __DIR__ . '/modal-image',
        array(
            'script' => 'modal-image-js',
        )
    );
    register_block_type( __DIR__ . '/hero' );
    register_block_type( __DIR__ . '/partner' );
    register_block_type( __DIR__ . '/ressource' );
    register_block_type( __DIR__ . '/modal-video' );
    register_block_type( __DIR__ . '/job-card' );
    register_block_type( __DIR__ . '/chiffre1' );
    register_block_type( __DIR__ . '/chiffre2' );
    register_block_type( __DIR__ . '/section-listing-article' );

}

add_action( 'wp_enqueue_scripts', 'ihag_blocks_scripts' );
function ihag_blocks_scripts() {
    // scripts 
    wp_register_script( 'modal-image-js', get_template_directory_uri() . '/blocks/modal-image/script.js' );
    wp_register_script( 'modal-video-js', get_template_directory_uri() . '/blocks/modal-video/script.js' );
    
    wp_register_script( 'newsletter-mailjet-js', get_template_directory_uri() . '/blocks/newsletter-mailjet/script.js' );
	

}

