<?php
/**
 * Block name : Job card
 */

$template = array(
    array(
		'core/heading',
		array(
			'placeholder' => 'Title',
			'level'       => 3,
		),
	),
    array( 'core/separator' ),
    array(
		'core/paragraph',
		array(
			'placeholder' => 'corps',
		),
	),
    array(
		'core/button',
		array(
			'placeholder' => 'test',
		),
	),
);

?>


<div class="job-card alignfull">
    <?php echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" />'; ?>
</div>
