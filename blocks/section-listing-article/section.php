<?php
/**
 * Block name : Section listing article
 */

$template = array(
    array(
		'core/heading',
		array(
			'placeholder' => 'titre',
			'level'       => 2,
		),
	),
    array( 'acf/listing-article' ),
    array(
		'core/button',
		array(
			'placeholder' => 'test',
		),
	),
);

?>


<div class="section-listing-article">
    <?php echo '<InnerBlocks template="' . esc_attr( wp_json_encode( $template ) ) . '" />'; ?>
</div>
