<?php

// insertion des cutoms post type
add_action( 'init', 'ihag_custom_post_property' );
function ihag_custom_post_property() {
    register_post_type( 
        'partner',
        array(
            'labels'             => array(
                'name'          => __( 'partners', 'ihag' ),
                'singular_name' => __( 'partner', 'ihag' ),
            ),
            'menu_position'      => 18,
            'menu_icon'          => 'dashicons-portfolio',
            'hierarchical'       => false,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'name' ),
            'show_in_rest'       => false,
            'has_archive'        => true,
            'supports'           => array( 'title', 'editor', 'thumbnail' ),
        )
    );
}
