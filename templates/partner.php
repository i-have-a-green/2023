<?php
/**
 * The template for displaying parters
 * 
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * 
 * @package ihag
 */

?>

    <div class="partner">
        <div class="picture">
            <?php the_post_thumbnail( 'thumbnail' ); ?>
        </div>
        <h2><?php the_title(); ?></h2>
        <div class="entry-content">
            <?php the_content(); ?> 
        </div>
    </div>
