<?php
/**
 * Title: Groupe de block chiffre/info
 * Slug: ihag/groupe-chiffre1
 * Categories:  global
 */
?>

<!-- wp:group {"align":"wide","className":"container-chiffre1","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide container-chiffre1"><!-- wp:acf/chiffre1 {"name":"acf/chiffre1","data":{},"mode":"auto"} -->
<!-- wp:heading {"placeholder":"Title"} -->
<h2>79% des entreprises</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>considèrent la gouvernance des données comme un enjeu majeur (Hubbi, 2020)</p>
<!-- /wp:paragraph -->
<!-- /wp:acf/chiffre1 -->

<!-- wp:acf/chiffre1 {"name":"acf/chiffre1","data":{},"mode":"auto"} -->
<!-- wp:heading {"placeholder":"Title"} -->
<h2>79% des entreprises</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>considèrent la gouvernance des données comme un enjeu majeur (Hubbi, 2020)</p>
<!-- /wp:paragraph -->
<!-- /wp:acf/chiffre1 -->

<!-- wp:acf/chiffre1 {"name":"acf/chiffre1","data":{},"mode":"auto"} -->
<!-- wp:heading {"placeholder":"Title"} -->
<h2>79% des entreprises</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>considèrent la gouvernance des données comme un enjeu majeur (Hubbi, 2020)</p>
<!-- /wp:paragraph -->
<!-- /wp:acf/chiffre1 --></div>
<!-- /wp:group -->
