<?php
/**
 * Title: Groupe de block chiffre/info 2
 * Slug: ihag/groupe-chiffre2
 * Categories:  global
 */
?>

<!-- wp:group {"align":"wide","className":"container-chiffre2","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignwide container-chiffre2"><!-- wp:acf/chiffre2 {"name":"acf/chiffre2","data":{},"mode":"auto"} -->
<!-- wp:heading {"placeholder":"Chiffre"} -->
<h2>25%</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"Information"} -->
<p>part des espèces animales et végétales connues menacées d'extinction</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"placeholder":"source"} -->
<p>IPBES, 2019</p>
<!-- /wp:paragraph -->
<!-- /wp:acf/chiffre2 -->

<!-- wp:acf/chiffre2 {"name":"acf/chiffre2","data":{},"mode":"auto"} -->
<!-- wp:heading {"placeholder":"Chiffre"} -->
<h2>25%</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"Information"} -->
<p>part des espèces animales et végétales connues menacées d'extinction</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"placeholder":"source"} -->
<p>IPBES, 2019</p>
<!-- /wp:paragraph -->
<!-- /wp:acf/chiffre2 -->

<!-- wp:acf/chiffre2 {"name":"acf/chiffre2","data":{},"mode":"auto"} -->
<!-- wp:heading {"placeholder":"Chiffre"} -->
<h2>25%</h2>
<!-- /wp:heading -->

<!-- wp:paragraph {"placeholder":"Information"} -->
<p>part des espèces animales et végétales connues menacées d'extinction</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph {"placeholder":"source"} -->
<p>IPBES, 2019</p>
<!-- /wp:paragraph -->
<!-- /wp:acf/chiffre2 --></div>
<!-- /wp:group -->
