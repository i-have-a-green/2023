<?php
/**
 * Title: Section Job Card
 * Slug: ihag/job-card-section
 * Categories: global
 */
?>

<!-- wp:group {"align":"full","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull section-job-card"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:heading -->
<h2>Des solutions data pour tous les métiers !</h2>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph -->
<p>L'exploitation des data est bénéfique pour tous les services de l'entreprise : elle est synonyme d'optimisation des processus et de décloisonnement des services. </p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:acf/jod-card {"name":"acf/jod-card","data":{},"mode":"preview"} -->
<!-- wp:heading {"level":3,"placeholder":"Title"} -->
<h3>Marketing</h3>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>De l’amélioration du parcours client au perfectionnement du ciblage, les données valent de l’or pour les services marketing.</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test"} -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">En savoir plus</a></div>
<!-- /wp:button -->
<!-- /wp:acf/jod-card --></div>
<!-- /wp:column --></div>
<!-- /wp:columns -->

<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:acf/jod-card {"name":"acf/jod-card","data":{},"mode":"preview"} -->
<!-- wp:heading {"level":3,"placeholder":"Title"} -->
<h3>Marketing</h3>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>De l’amélioration du parcours client au perfectionnement du ciblage, les données valent de l’or pour les services marketing.</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test"} -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">En savoir plus</a></div>
<!-- /wp:button -->
<!-- /wp:acf/jod-card --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:acf/jod-card {"name":"acf/jod-card","data":{},"mode":"preview"} -->
<!-- wp:heading {"level":3,"placeholder":"Title"} -->
<h3>Marketing</h3>
<!-- /wp:heading -->

<!-- wp:separator -->
<hr class="wp-block-separator has-alpha-channel-opacity"/>
<!-- /wp:separator -->

<!-- wp:paragraph {"placeholder":"corps"} -->
<p>De l’amélioration du parcours client au perfectionnement du ciblage, les données valent de l’or pour les services marketing.</p>
<!-- /wp:paragraph -->

<!-- wp:button {"placeholder":"test"} -->
<div class="wp-block-button"><a class="wp-block-button__link wp-element-button">En savoir plus</a></div>
<!-- /wp:button -->
<!-- /wp:acf/jod-card --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
