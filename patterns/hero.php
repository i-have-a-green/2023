<?php
/**
 * Title: entête avec H1/paragraphe + image
 * Slug: ihag/hero
 * Categories:  global
 */
?>

<!-- wp:group {"align":"full","className":"hero","layout":{"type":"constrained"}} -->
<div class="wp-block-group alignfull hero"><!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide"><!-- wp:column -->
<div class="wp-block-column"><!-- wp:heading {"level":1} -->
<h1>Titre de la page !</h1>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Vivamus a tempus neque. Mauris aliquet est finibus, feugiat odio porttitor, ornare ligula. Nullam vel risus facilisis, molestie elit eget, volutpat turpis. Fusce id accumsan nisl. Nunc iaculis orci sem, vel molestie sapien fermentum eget.</p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->

<!-- wp:column -->
<div class="wp-block-column"><!-- wp:image {"id":243,"sizeSlug":"large","linkDestination":"none"} -->
<figure class="wp-block-image size-large"><img src="http://startertheme.local/wp-content/uploads/2022/10/image-base-1024x576.jpeg" alt="" class="wp-image-243"/></figure>
<!-- /wp:image --></div>
<!-- /wp:column --></div>
<!-- /wp:columns --></div>
<!-- /wp:group -->
