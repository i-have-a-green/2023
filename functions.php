<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Armando
 * @since 1.0.0
 */

/**
 * The theme version.
 *
 * @since 1.0.0
 */

define( 'IHAG_VERSION', wp_get_theme()->get( 'Version' ) );
require_once get_template_directory() . '/functions_cpt_taxo.php';
require_once get_template_directory() . '/functions_image.php';
require_once get_template_directory() . '/functions_required.php';
require_once get_template_directory() . '/blocks/blocks.php';

add_action( 'wp_enqueue_scripts', 'ihag_scripts' );
function ihag_scripts() {
    // styles

    wp_enqueue_script( 'ihag-script', get_template_directory_uri() . '/assets/script.js', array(), IHAG_VERSION, true );
    wp_localize_script(
        'ihag-script',
        'ihagRest',
        array(
			'root'  => esc_url_raw( rest_url() ),
			'nonce' => wp_create_nonce( 'wp_rest' ),
            'resturl' => site_url() . '/wp-json/ihag/' 
        ) 
    );

}

/************* Page option ACF */
if ( function_exists( 'acf_add_options_page' ) ) {
	// Page principale
	acf_add_options_page(
        array(
			'page_title'    => 'Options',
			'menu_title'    => 'Options',
			'menu_slug'     => 'options-generales',
			'capability'    => 'edit_posts',
			'redirect'      => true,
        )
    );

    // Page d'options
    acf_add_options_sub_page(
        array(
			'page_title'    => 'Options Générales',
			'menu_slug'     => 'acf_options',
			'parent_slug'   => 'options-generales',
        )
    );
}


/*add_action( 'init', 'ihag_add_theme_support' );
function ihag_add_theme_support() {
    add_theme_support( 'responsive-embeds' );
}*/

/*
 add_action( 'wp_after_insert_post', function( $post_id, $post ) {
    if ( $post->post_type !== 'wp_global_styles' ) {
        // export theme.json
	$tree = WP_Theme_JSON_Resolver::get_theme_data( array(), array( 'with_supports' => false ) );
	// Merge with user data.
	$tree->merge( WP_Theme_JSON_Resolver::get_user_data() );
    $theme = $tree->get_data();

    file_put_contents(__DIR__.'/theme.json',json_encode($theme));
} */


function excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'excerpt_length');


function check_nonce() {
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}




/**
 * My_acf_json_save_point
 *
 * @param  mixed $path
 * @return string
 */
function ihag_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/jsonACF';
	if ( ! file_exists( $path ) ) {
		mkdir( $path, 0777 );}
	return $path;
}
add_filter( 'acf/settings/save_json', 'ihag_acf_json_save_point' );


/**
 * My_acf_json_load_point
 *
 * @param  mixed $paths
 * @return string
 */
function ihag_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/jsonACF';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'ihag_acf_json_load_point' );


if ( ! defined( 'WP_POST_REVISIONS' ) ) {
	define( 'WP_POST_REVISIONS', 3 );
}

function ihag_revision_number( $num, $post ) {
	return 3;
}
add_filter( 'wp_revisions_to_keep', 'ihag_revision_number', 4, 2 );
