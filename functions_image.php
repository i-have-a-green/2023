<?php

function ihag_add_image_size() {
	
	add_image_size( '450-450', 450, 450 );
	add_image_size( '650-650', 650, 650 );
	add_image_size( '850-850', 850, 850 );

	function ihag_custom_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'450-450' => __( '450x450 - Moyen sans rognage', 'ihag' ),
				'650-650' => __( '650x650 - Grande sans rognage', 'ihag' ),
				'650-650' => __( '850x850 - Grande sans rognage', 'ihag' ),
			)
		);
	}
	add_filter( 'image_size_names_choose', 'ihag_custom_sizes' );

}
add_action( 'after_setup_theme', 'ihag_add_image_size' );
